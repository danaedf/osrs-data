# Osrs Data

Data about OSRS used in runemate bots.

Essentially a Java implementation of https://github.com/osrsbox/osrsbox-db

Only monsters are supported for now. Compared to osrsbox, the monster image and xp_bonus parameters were added. This also fixes some bug osrsbox had with some monsters not being added because of the wiki infobox hierarchy.